﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Working_With_Files_Directories_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //lets go ahead and call this method that will create new directory
            create_new_directory();

            //when running this program, best to use a break point after this comment to check out the directory that was created.
            delete_a_directory();

            Console.ReadLine();
        }

        //this method will delete a directory
        private static void delete_a_directory()
        {
            //lets create a directory object with a path
            var directory_object = new DirectoryInfo(@"C:\Users\Administrator\Documents\implementdataaccess\directory_playground\create1");

            //deleting the directory
            try
            {
                directory_object.Delete();
                Console.WriteLine("Program.cs - create_new_directory - directory deletion succeeded");
            }
            catch (Exception e)
            {
                Console.WriteLine("Program.cs - create_new_directory - directory deletion failed - {0}", e.ToString());
            }
        }

        //this method will create a directory
        private static void create_new_directory()
        {
            //lets create a directory object with a path
            var directory_object = new DirectoryInfo(@"C:\Users\Administrator\Documents\implementdataaccess\directory_playground\create1");

            //creating the directory
            try
            {
                directory_object.Create();
                Console.WriteLine("Program.cs - create_new_directory - directory creation succeeded");

                //after the folder is created, it will have the access levels of the current user. 
                //of course the access levels can be modified

                //creating a security level object
                //and then obtaining the existing acccess control of the directory that was created above
                DirectorySecurity security_level_object = directory_object.GetAccessControl();
                //now modify the access control so that everybody can access this file not just the user who created it
                //here, I have created a accessrule called 'everyone' and then assigned it some access privileges
                //now I can use this access rule to any directory to which i have rights to modify stuff
                security_level_object.AddAccessRule(new FileSystemAccessRule("everyone", FileSystemRights.ReadAndExecute, AccessControlType.Allow));

                //now setting the access level of the directory that was just created
                directory_object.SetAccessControl(security_level_object);
                Console.WriteLine("Program.cs - create_new_directory - directory access level applied");
            }
            catch(Exception e)
            {
                Console.WriteLine("Program.cs - create_new_directory - directory creation failed - {0}", e.ToString());
            }

        }
    }
}
